/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// Dependencies
import VueRouter from 'vue-router';
import { Form, HasError, AlertError } from 'vform';
import VueProgressBar from 'vue-progressbar';
import Swal from 'sweetalert2'
import { Circle, HourGlass } from 'vue-loading-spinner';
import Toastr from 'vue-toastr';
import VueLazyload from 'vue-lazyload'
import { VueEditor } from "vue2-editor";
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import UserModal from './components/user/modal/UserModal.vue';
// Initialize

let routes = [
    { 	
		path: '/home', 
		component: ()=>import( /* webpackChunkName: "Dashboard" */ "./components/DashboardComponent.vue") 
    },
    { 
		path: '/category', 
		component: ()=>import( /* webpackChunkName: "Category" */ "./components/category/CategoryComponent.vue") 
    },
    { 
		path: '/subcategory', 
		component: ()=>import( /* webpackChunkName: "Subcategory" */ "./components/subcategory/SubcategoryComponent.vue")
    },
    { 
		path: '/products', 
		component: ()=>import( /* webpackChunkName: "Products" */ "./components/products/ProductComponent.vue") 
    },
    { 
		path: '/users', 
		component: ()=>import( /* webpackChunkName: "Users" */ "./components/user/UserComponent.vue") 
    },
    { 
		path: '/orders', 
		component: ()=>import(/* webpackChunkName: "Orders" */ "./components/orders/OrderComponent.vue") 
    },
    { 
		path: '/payments', 
		component: ()=>import( /* webpackChunkName: "Payments" */ "./components/payments/PaymentComponent.vue") 
    },
    { 
		path: '/productDetails/:productId', 
		name : 'productDetails', 
		component: ()=>import( /* webpackChunkName: "ProductDetails" */ "./components/productDetails/ProductDetails.vue")
    },
    { 
		path: '/activity', 
		component: ()=>import( /* webpackChunkName: "Activity" */ "./components/activity/ActivityComponent.vue") 
	},  

	{ 
		path: '/discount', 
		component: ()=>import( /* webpackChunkName: "Product Discount" */ "./components/products/DiscountComponent.vue") 
    },  
  ]

const router = new VueRouter({
    mode : 'history',
    routes
});
const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000
});

window.Form = Form;
window.Swal = Swal;
window.Toast = Toast;
window.Fire = new Vue();
// Use of Components

Vue.use(Toastr);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(VueProgressBar, {
	color: 'rgb(143, 255, 199)',
	failedColor: 'red',
	height: '3px'
});
Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: 'dist/error.png',
	loading: 'https://thumbs.gfycat.com/SizzlingSmallAbalone-small.gif',
	attempt: 1
})

Vue.filter('currency', function (value) {
  return '₱' + parseFloat(value).toFixed(2);
});

Vue.filter('discount', function(value){
	return value * 100;
})





/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component(
	'header-content', 
	()=>import( /* webpackChunkName: "HeaderContent" */ "./components/ContentHeader.vue")
);
Vue.component(
	'editcategory-modal', 
	() => import( /* webpackChunkName: "EditCategoryModal" */ "./components/category/modal/EditCategoryModal.vue")
);
Vue.component(
	'subcategory-modal', 
	() => import( /* webpackChunkName: "SubcategoryModal" */ "./components/subcategory/modal/SubCategoryModal.vue")
);
Vue.component(
	'product-modal',
	() => import( /* webpackChunkName: "ProductModal" */ "./components/products/modal/ProductModal.vue"));
Vue.component(
	'user-modal', 
	()=> import( /* webpackChunkName: "UserModal" */"./components/user/modal/UserModal.vue")
);
Vue.component(
	'loading', 
	() => import( /* webpackChunkName: "Loading" */ "vue-full-loading")
);
Vue.component(
	'pagination', 
	require( /* webpackChunkName: "Pagination" */ 'laravel-vue-pagination')
);
Vue.component(
	'multi-dropdown', 
	()=>import('./components/products/modal/Filter.vue')
);
Vue.component(
	'image-cropper',
	 () => import( /* webpackChunkName: "ImageCropper" */ "./components/products/modal/ImageCropper.vue")
);
Vue.component(
	'image-modal', 
	() => import( /* webpackChunkName: "ImageModal" */ "./components/products/modal/ProductImageModal.vue")
);
Vue.component(
	'home-carousel', 
	() => import( /* webpackChunkName: "HomeCarousel" */ './components/Carousel.vue')
);
Vue.component(
	'sample-image', 
	() => import( /* webpackChunkName: "SampleImage" */ './components/Home_Product.vue')
);
Vue.component(
	'register-modal', 
	()=> import( /* webpackChunkName: "RegisterModal" */ './components/Register.vue')
);
Vue.component(
	'activity-modal', 
	()=> import( /* webpackChunkName: "ActivityModal" */ './components/activity/modal/ActivityModal.vue')
);
Vue.component(
	'add-cart', 
	()=>import( /* webpackChunkName: "AddCart" */ './components/addToCart.vue')
);
Vue.component(
	'order-items', 
	() => import( /* webpackChunkName: "ProductModal" */ "./components/orders/modal/OrderItemsModalComponent.vue")
);

Vue.component(
	'discount-items', 
	() => import( /* webpackChunkName: "ProductModal" */ "./components/products/modal/DiscountComponent.vue")
);
Vue.component('vue-editor',VueEditor)
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component('circle-spinner', Circle);
Vue.component('hour-glass', HourGlass);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
