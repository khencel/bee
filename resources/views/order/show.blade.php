@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row mt-3">
           
            @foreach($orders as $order)
                <div class="col-12">
                    <div class="rounded-0 bg-white my-2" style="box-shadow: 0px 0px 3px black;" >
                        <div class="pl-2 pb-2 pt-1" style="border-bottom: 1px solid black;">
                            <span class="font-weight-bold" style="letter-spacing: 2px;">Order </span>  #{{ $order->order_no }} <span style="letter-spacing: 2px;" class="badge badge-info p-2 rounded text-white">{{ $order->status }}</span><br>
                            <span class="text-muted" style="font-size: 14px;"> Placed on {{ $order->created_at }}</span> 
                            
                        </div>
                        <div>
                            <table class="table table-striped table-hover">
                                <thead class="bg-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Variant</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                        @foreach($order->order_details as $orderDetail)
                                            <tr>
                                                <td>
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td class="p-2">
                                                    <img src='/img/product-images/{{ $orderDetail->product_details->products->product_images[0]->image }}' alt="{{ $orderDetail->product_details->products->name }}" style="height: 100px; width: 100px;">
                                                    <span>{{ $orderDetail->product_details->products->name }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <span class="font-weight-bold">P{{ $orderDetail->product_details->products->price }}</span>
                                                </td>
                                                <td>
                                                    {{ $orderDetail->product_details->color }}
                                                </td>
                                                <td>
                                                    {{ $orderDetail->quantity }}
                                                </td>
                                                <td>
                                                    P{{ $orderDetail->price }}
                                                </td>
                                            </tr>
                                        @endforeach  
                                    </tbody>
                                </table>
                        </div>
                       
                    </div>
                </div>
            @endforeach
            
        </div>
    </div>
    
@endsection