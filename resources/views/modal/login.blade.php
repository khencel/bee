<form action="/login/customer" method="post">
    @csrf
<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class=" h4 text-center p-4">
                <span class="orange-font">
                    Welcome!
                </span> 
                <span class="green-font">
                    Please Login
                </span> 
                
               
          </div>
          <div class="row justify-content-center">
                <div class="col-md-10 mt-2">
                    <span class="form-group has-float-label">
                            <input id="email" type="email" class="material-textbox form-control @error('email') is-invalid @enderror" name="email" placeholder=" " value="{{ old('email') }}" required autocomplete="email" >
                            <label for="email">Email</label>
                    </span>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-10 mt-2">
                    <span class="form-group has-float-label">
                            <input id="password" placeholder=" " type="password" class="material-textbox form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            <label for="email">Password</label>
                    </span>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-10 text-right">
                    <small class="font-weight-bold" style="color:#6cb2eb;letter-spacing:1px;">Forgot Password?</small>
                </div>
                <div class="col-md-10 mt-4">
                    <button type="submit" class="w-100 button-login p-1 orange">Login</button>
                </div>
                <div class="col-md-10 p-3 text-center">
                    or login with
                </div>
                <div class="col-md-10">
                    <a href="/login/facebook?path=<?php echo url()->current() ?>" class="btn form-control button-login p-1 bg-facebook text-white"><i class="fab fa-facebook-square" aria-hidden="true"></i> facebook</a>
                </div>
                <hr style="width:80%;background-color:gray;height:2px;">
                <div class="col-md-10 p-2 text-right">
                    not a member? <a href="" id="btnRegister" data-dismiss="modal"><span style="color:#6cb2eb;">Sign Up</span></a>  
                </div>
          </div>
        </div>
  </div>
</div>
</form>