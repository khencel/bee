<form action="addshipping" method="post">
    @csrf
    <div class="modal fade" id="shipping" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="h4 text-center p-2">
                            <span>Add 
                            <span class="orange-font">Shipping </span> 
                            and 
                            <span class="green-font"> Billing</span>
                            Information
                            </span>
                    </div>
                    
                    <div class="row  p-4">
                        <label for="">Billing Address</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                            <div class="input-group-text">
                            <i class="fa fa-address-book nav-icon" aria-hidden="true"></i>
                            </div>
                            </div>
                            <input type="text" class="form-control" name="address" aria-label="Text input with checkbox">
                        </div>

                        <label for="">Contact Number</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                            <div class="input-group-text">
                            <i class="fa fa-mobile nav-icon" aria-hidden="true"></i>
                            </div>
                            </div>
                            <input type="text" class="form-control" name="mobile" aria-label="Text input with checkbox">
                        </div>

                        <label for="">Email Address</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            </div>
                            <input type="text" class="form-control" name="email" aria-label="Text input with checkbox">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success w-100">Save</button>
            </div>
        </div>
    </div>
</div>
</form>