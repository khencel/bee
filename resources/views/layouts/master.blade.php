<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>HappyBee</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Questrial&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.rawgit.com/tonystar/bootstrap-float-label/v3.0.1/dist/bootstrap-float-label.min.css"/>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/bee.png') }}"/>

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css'/>
    @yield('style')
</head>
<body>
    <div id="app" style="overflow-x:hidden;">
            @include('partial.navigation')
            
            @yield('content')
            
            <div id="divFooter">
                @include('partial.footer')
            </div>
            
            
    </div>
    {{-- modal --}}
    @include('modal.login')
    
    {{-- endmodal --}}
    
    <script src="{{ asset('js/app.js') }}"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
    <script src="{{ asset('js/dobpicker.js') }}"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <script>
        $(document).ready(function(){
            var pathname = window.location.pathname;

            if(pathname == '/colors'){
                $('#colorsPrints').addClass('active');
                $('#bg-colorsPrint').addClass('nav-active');
                $('#header-format').removeClass('orange');
                $('#header-format').addClass('green');
                $('#bg-nav').removeClass('green');
                $('#bg-nav').addClass('orange');
                $('#bg-footer').removeClass('green');
                $('#bg-footer').addClass('orange');
                $('#bg-bottom-footer').removeClass('orange');
                $('#bg-bottom-footer').addClass('green');
            }

            if(pathname == '/foodies'){
                $('#colorsfoodies').addClass('active');
                $('#bg-foodies').addClass('nav-active');
            }

            if(pathname == '/outfit'){
                $('#colorsOutfit').addClass('active');
                $('#bg-outfit').addClass('nav-active');
                $('#header-format').removeClass('orange');
                $('#header-format').addClass('green');
                $('#bg-nav').removeClass('green');
                $('#bg-nav').addClass('orange');
                $('#bg-footer').removeClass('green');
                $('#bg-footer').addClass('orange');
                $('#bg-bottom-footer').removeClass('orange');
                $('#bg-bottom-footer').addClass('green');
            }

            $("#login-button").click(function(event){
                event.preventDefault()
                $('#loginmodal').modal('show');
            })
            
            $('#btnRegister').click(function(){  
                $('#registerModal').modal('show');
            })
            // $('#registerModal').modal('show');
            $('#btnAddToCart').click(function(event){
                event.preventDefault()
                var check = '{{Auth::user()}}'
                if(check){
                    $('#addcart-modal').modal('show');
                }
            })

            
            function cartCounter(){
                $.ajax({
                    url:'/cartCounter',
                    method:'POST',
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(data){
                        if(data <= 0){
                            $('#cartQuantity').html();
                        }else{
                            $('#cartQuantity').html(data);
                        }
                    }
                });
            }

            cartCounter();
            
        })
    </script>
    @yield('script')
</body>
</html>