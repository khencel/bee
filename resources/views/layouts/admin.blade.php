
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name = "csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLNhLioJNwOFfa_-Oig2xUOZ0WWIs1U626qPGFtIR6Anktq8PqzQ" type="image/x-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <title>Happy Bee</title>

 <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id = "app">
  @if(Auth::user()->type == 'admin')
    @include('partial.admin.navigation')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <router-view></router-view>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Powered by <strong> <a  href="https://naotech.com.ph/">Naotech Inc</a> </strong>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="http://happybee.com.ph">Happy Bee</a>.</strong> All rights reserved.
  </footer>
  @else
    <script>
      alert("Unauthorized");
      window.location.href = "/";
    </script>
  @endif
</div>



<script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
