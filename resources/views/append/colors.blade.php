@foreach($product as $product)
            <div class="col-lg-4 col-md-6 col-6">
                <div class="row p-2">
                    <div class="col-lg-6 product-container text-center">
                        <img class="img-fluid" src="{{ asset('img/'.$loop->iteration.'.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 bg-item-details">
                        <div class="h5 text-white mt-3">
                            {{$product->product}}
                        </div>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </div>
                        <div class="h5 mt-2">
                            Item Price (P199)
                        </div>
                        <div class="text-center pb-3">
                            
                                <span>
                                        <button class="green btn mb-1" style="border:none;border-radius:10px;font-size:12px;">ORDER NOW</button>
                                </span>
                                <span>
                                        <button class="orange btn" style="border:none;border-radius:10px;font-size:12px;">ADD TO CART</button>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
@endforeach