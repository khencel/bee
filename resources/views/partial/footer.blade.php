<div class="row p-5">
    <div class="col-md-6">
        <div class="row justify-content-center">
            <div class="col-md-8 connect-social">
                <div class=" font-weight-bold orange-font" >
                    <h1 style="line-height:10px;">Connect With Us</h1>
                    <h3 class="float-right">on Social Media</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="mb-2">
                    <a href="https://www.facebook.com/mapfre.insular.33">
                        <i class="fab fa-facebook-square fa-2x" style="color:rgb(51,102,153);"></i><span class="ml-2 social-font" >@mapfre.insular.33</span>
                    </a>
                </div>
                <div class="mb-2">
                    <a href="https://www.facebook.com/beegoodies2830">
                            <i class="fab fa-facebook-square fa-2x" style="color:rgb(51,102,153);"></i><span class="ml-2 social-font" >@beegoodies2830</span>
                    </a>
                </div>
                <div class="mb-2">
                    <a href="https://www.facebook.com/beegoodies.outfit">
                            <i class="fab fa-facebook-square fa-2x" style="color:rgb(51,102,153);"></i><span class="ml-2 social-font" >@beegoodies.outfit</span>
                    </a>
                </div>
                <div class="mb-2">
                    <a href="https://www.facebook.com/beegoodies.home.9">
                            <i class="fab fa-facebook-square fa-2x" style="color:rgb(51,102,153);"></i><span class="ml-2 social-font" >@beegoodies.home.9</span>
                    </a>
                </div>
                <div class="mb-2">
                    <a href="https://www.facebook.com/B-Shoes-834339423582366">
                            <i class="fab fa-facebook-square fa-2x" style="color:rgb(51,102,153);"></i><span class="ml-2 social-font" >@B-Shoes-834339423582366</span>
                    </a>  
                </div>
                <div class="mb-2">
                    <a href="">
                        <a href="https://www.instagram.com/happybee/">
                                <i class="fab fa-instagram fa-2x" style="color:rgb(219,182,109);"></i><span class="ml-2 social-font" >@happybee</span>
                        </a>
                    </a>
                </div>
                <div class="mb-2">
                    <a href="">
                            <i class="fab fa-youtube fa-2x" style="color:rgb(204,51,0);"></i><span class="ml-2 social-font" >@happybee</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="green text-center" id="bg-footer" style="height:30px;">
    © {{ date('Y') }} Copyright: Naotech
</div>
<div class="" style="height:10px;background-color:rgb(153, 204, 102)">
</div>
<div class="orange text-center pt-3" id="bg-bottom-footer">
<div class="header-bg-div m-auto">
    <img class="img-fluid" src="{{ asset('img/bg-header.png') }}" alt="">
</div>
</div>