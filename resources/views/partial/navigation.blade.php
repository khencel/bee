<div class="orange text-center pt-3" id="header-format">
    <div class="text-white cart-div">
        <div class="" style="height:60px;">
          
            
            {{-- {{Auth::user()->name}} --}}
            @if (Auth::user() == null)
            <div class="float-left p-1">
              <i class="fa fa-user" style="font-size:30px;" aria-hidden="true"></i>
            </div>
            <a href="" id="login-button" disabled="disabled" class="text-white">
                <div class="float-left">
                  <div style="font-size:14px;font-weight:bold;">
                    Sign In
                  </div>
                  <div style="font-size:10px;">
                    My Account 
                  </div>
                </div>
            </a>
			@else
        
					  <div class="float-left">
              <div style="font-size:17px;font-weight:bold;">
                  <a href="/+order/{{ Auth::user()->id }}" class="text-white"> My Orders</a>
              </div>
					  </div>
            <div class="float-left p-1">
              <img class="img-responsive rounded-circle" style="width:35px;" src="{{ Auth::user()->avatar }}" alt="">
            </div>
                <div class="float-left">
                  <div style="font-size:14px;font-weight:bold;" id="dropdown">
                    {{ Auth::user()->name }}
                  </div>
                  <div style="font-size:10px;">
                      <div class="">
                          <a class="text-white" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
						  </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </div>
                </div>

                
            @endif
            
          
          <a href="" id="btnAddToCart" class="text-white">
              <div class="float-left p-1 ml-3">
                <i class="fa fa-shopping-cart" style="font-size:30px;" aria-hidden="true"></i>
                <span style="position:absolute;top:25px;font-size:12px;right:30px;" id="cartQuantity" class="badge badge-primary"></span>
              </div>
              <div class="float-left">
                <div style="font-size:14px;font-weight:bold;">
                  Cart
                </div>
                <div style="font-size:10px;">
                  Items
                </div>
              </div>
          </a>
        </div>
    </div>
    <div class="search-div">
            <label class="sr-only" for="inlineFormInputGroupUsername">Search</label>
            <div class="input-group">
              <input type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="Search">
              <div class="input-group-prepend" >
                <div class="input-group-text" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;">
                        <i class="fas fa-search"></i>
                </div>
              </div>
            </div>
    </div>
    <div class="header-bg-div">
      <a href="/">
        <img style="position:absolute;width:30%;left:35%;" src="{{ asset('img/HappyBeeText.png') }}" alt="">
      </a>
    </div>
    <div class="header-bg-div m-auto">
        <img class="img-fluid" src="{{ asset('img/bg-header.png') }}" alt="">
    </div>
    
</div>
<nav class="navbar navbar-expand-lg navbar-dark nav-color p-0 green" id="bg-nav">
    {{-- <a class="navbar-brand" href="#">Navbar</a> --}}
    <button class="navbar-toggler ml-auto" style="border:none;" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav m-auto text-uppercase">
        <li class="nav-item mr-5 hover-design active" id="bg-colorsPrint">
          <a class="nav-link" id="colorsPrints" href="/productCategory/1?page=" target="_blank">HappyBee Colors & Prints <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item mr-5 hover-design active" id="bg-foodies">
          <a class="nav-link" id="colorsfoodies" href="/productCategory/2?page=" target="_blank">BeeGoodies Foodies</a>
        </li>
        <li class="nav-item mr-5 hover-design active" id="bg-outfit">
          <a class="nav-link" id="colorsOutfit" href="/productCategory/3?page=" target="_blank">BeeGoodies Outfit</a>
        </li>
        <li class="nav-item mr-5 hover-design active">
          <a class="nav-link" href="/productCategory/4?page=">BeeGoodies Home</a>
        </li>
        <li class="nav-item mr-5 hover-design active">
          <a class="nav-link" href="/productCategory/5?page=">B Shoes</a>
        </li>
        <li class="nav-item mr-5 hover-design active">
          <a class="nav-link" href="/productCategory/6?page=">Other Bee Business</a>
        </li>
        <li class="nav-item mr-5 dropdown hover-design active">
          <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Track My Order</a>
          <div class="dropdown-menu text-capitalize" aria-labelledby="dropdownMenuButton">
            <div class="dropdown-item" style="width:300px;">
                <div>
                    Tracking
                </div>
                <div style="font-size:10px;">
                    Please confirm your email:
                </div>
                <div>
                    <input type="text" class="form-control">
                </div>
                <br>
                <div style="font-size:10px;">
                    Your order number:
                </div>
                <div>
                    <label class="sr-only" for="inlineFormInputGroupUsername">Search</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="">
                      <a href="#">
                      <div class="input-group-prepend">
                        <div class="input-group-text" >
                               >
                        </div>
                      </div>
                      </a>
                    </div>
                </div>
                <div style="font-size:10px;">
                    For any other inquiries, click here
                </div>
            </div>
          </div>
        </li>
        <li class="nav-item mr-5 hover-design active">
          <a class="nav-link" href="#">Contact Us</a>
        </li>
      </ul>
    </div>
  </nav>
  
  <register-modal  status="hide" id="registerModalC"></register-modal>
  
  @if (isset(Auth::user()->id))
    <add-cart :user_id="{{ Auth::user()->id }}"></add-cart>
  @else
    <add-cart user_id="" ></add-cart>
  @endif

  
  