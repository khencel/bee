 <!-- Navbar -->

 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>
  
    {{-- <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> --}}
  
    {{--  <!-- Right navbar links -->  --}}
      <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown" style="color:black;  font-size:15px; !important">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->email }} <span class="caret"></span>
              </a>
  
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="width:230px;">
                  <div class="container-fluid">
                      <div class="row mb-2   ">
                          <div class="col-md-12 mb-3 mt-3 text-center">
                                  <img class="img-responsive rounded-circle"  width="100" height="100" src="https://png.pngtree.com/element_our/png/20180912/coffee-time-png_91570.jpg"
                                  alt="User picture">
                          </div>
                          <span class="mx-auto mb-3">Adminsitrator</span>
                      </div>
  
                      <div class="row">
                              <div class="col-md-6 float-left">
                                      <a href="/users/{{ Auth::user()->id }}" class="btn btn-primary p-1 text-bold" >Edit Profile</a>
                              </div>
                                  <div class="col-md-6 text-right ">
                                      <a class="btn btn-danger p-1" href="{{ route('logout') }}"
                                      onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                          {{ __('Sign Out') }}
                                      </a>
  
                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                      </form>
                                  </div>
                      </div>
  
                  </div>
  
              </div>
          </li>
      </ul>
  </nav>
  {{--  <!-- /.navbar -->  --}}
  
  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLNhLioJNwOFfa_-Oig2xUOZ0WWIs1U626qPGFtIR6Anktq8PqzQ" alt="HappyBee Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">HappyBee</span>
    </a>
  
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://png.pngtree.com/element_our/png/20180912/coffee-time-png_91570.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }} </a>
        </div>
      </div>
  
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
        <router-link to="/home" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt active"></i>
          <p>
          Dashboard
          </p>
        </router-link>
        <router-link to="/category" class="nav-link">
          <i class="fab fa-cuttlefish nav-icon"></i>
          <p>
          Category
          </p>
        </router-link>
        <router-link to="/subcategory" class="nav-link">
          <i class="fa fa-list-ul nav-icon" aria-hidden="true"></i>
          <p>
          SubCategory
          </p>
        </router-link>
        <router-link to="/products" class="nav-link">
          <i class="fas fa-box-open   nav-icon "></i>
          <p>
          Products
          </p>
        </router-link>
  
          
        <router-link to="/users" class="nav-link">
          <i class="fa fa-users nav-icon" aria-hidden="true"></i>
          <p>
            User Management
          </p>
        </router-link>
        
        <router-link to="/orders" class="nav-link">
          <i class="fa fa-bookmark nav-icon" aria-hidden="true"></i> 
          <p>
          Orders
          </p>
        </router-link>
        <router-link to="/payments" class="nav-link">
          <i class="fas fa-dollar-sign  nav-icon  "></i>
          <p>
            Payments
          </p>
        </router-link>
        <router-link to="/activity" class="nav-link">
          <i class="fas fa-paste  nav-icon  "></i>
          <p>
            Activity
          </p>
        </router-link>

        <router-link to="/discount" class="nav-link">
          <i class="fa fa-bookmark nav-icon" aria-hidden="true"></i> 
          <p>
          Product Discount
          </p>
        </router-link>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  	</aside>

 
