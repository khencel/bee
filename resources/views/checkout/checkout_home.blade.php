@extends('layouts.master')

@section('content')
    <div class="container mt-4 mb-0">
        @if($errors->has('cart'))
            <div class="alert alert-danger font-weight-bold">
                Please select items/orders in your cart first.
            </div>
        @endif
    </div>
    <div class="row p-4 justify-content-center" style="background-color:linen;">
     
        <div class="col-md-3 p-2 bg-white">
            <div class="row justify-content-center">
                <div class="col-md-4 text-center p-2">
                        <img style="width:50px;" src="{{ asset('img/cash-on-delivery.png') }}" alt="">
                        <p>
                            Cash on Delivery   
                            <input  type="radio" checked name="payMethod"> 
                        </p> 
                </div>
                <div class="col-md-4 text-center p-2">
                        <img style="width:50px;" src="{{ asset('img/debit-card.png') }}" alt="">
                        <p>
                            Credit/Debit Card
                            <input  type="radio" disabled name="payMethod"> 
                        </p> 
                </div>
            </div>
            <div class=" text-center p-2 h5 font-weight-bold">
              Shipping and Billing Information
            </div>
            <div class="font-weight-bold p-1">
                <span class="mr-1"><i class="fa fa-user green-font" aria-hidden="true"></i></span>{{ Auth::user()->name }}
            </div>
            
            <div class="p-1">
                <p class="ml-3 address">{{ (isset(Auth::user()->user_details->address) && Auth::user()->user_details->address != null) ? Auth::user()->user_details->address : 'No Shipping Address..' }}</p>
            </div>
            <div class="p-1 font-weight-bold">
                <span class="mr-1"><i class="fa fa-phone green-font" aria-hidden="true"></i></span>
                <span id="userContact">{{ (isset(Auth::user()->user_details->mobile) && Auth::user()->user_details->mobile != null) ? Auth::user()->user_details->mobile : 'Add Mobile' }}</span>
            </div>
            <div class="p-1 font-weight-bold">
                <span class="mr-1"><i class="fa fa-envelope green-font" aria-hidden="true"></i></span> {{ (isset(Auth::user()->user_details->email)) ? Auth::user()->user_details->email : 'Add Email' }}
            </div>
            <div>
                @if(empty(\Auth::user()->user_details))
                    <button class="form-control green" data-toggle="modal" data-target="#shipping"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Shipping Details</button>
                @else
                    <button class="form-control green" data-toggle="modal" data-target="#shipping"><i class="fa fa-plus-circle" aria-hidden="true"></i> Edit Shipping Details</button>
                @endif
                
               
            </div>
            <div class="p-1 h5 font-weight-bold">
                Order Summary
            </div>
            <div class="p-1">
                Subtotal (<span id="quantity">0</span> item/s) 
                <span class="float-right grandtotal">₱0</span>
            </div>
            <div class="p-1 font-weight-bold">
               <h3>Total:
                <span class="float-right grandtotal">₱0</span>
            </h3> 
            </div>
        </div>



        <div class="col-md-7 bg-white p-3" style="border-left:1px solid;border-color:gray;">
            <form action="placeorder" id="placeOrderSubmit" method="post">
                @csrf
            <div class="row">
                <div class="col-md-1 font-weight-bold gray">       
                    Order
                </div>
                <div class="col-md-7 font-weight-bold gray">       
                    ITEM
                </div>
                <div class="col-md-2 text-center font-weight-bold gray">
                    Price/pc
                </div>
                <div class="col-md-2 text-center font-weight-bold gray">
                    Quantity
                </div>
            
            
                    @forelse($myCart->cart as $key => $cart)
                        <div class="col-md-8 ">    
                            <div class="row">
                                <div class="col-md-1 ">
                                    <div class="form-check text-center">
                                        <input type="checkbox" class="form-check-input" name="cart[]" id="{{ $cart->id }}" value="{{ $cart->id }}" onclick="addCart({{ $cart->product_details->products->price }}, {{ $cart->quantity }}, {{ $cart->id }})">
                                    </div>
                                </div>
                                <div class="col-md-3 p-2 ">

                                    <img class="img-fluid" src="{{ asset('/img/product-images/'. $cart->product_details->products->product_images[0]->image ) }}" alt="">
                                </div>
                                <div class="col-md-8 font-weight-bold gray ">
                                    {{ $cart->product_details->products->name }}
                                    <br>
                                    <br>
                                    <span class="font-italic font-weight-bold" style="color: {{ $cart->product_details->color }}">{{ $cart->product_details->color }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 text-center ">
                                ₱@convert($cart->product_details->products->price)
                        </div>
                        <div class="col-md-2 text-center">
                            {{ $cart->quantity }}
                        </div>
                        <hr>
                    @empty
                    <div class="col-md-12
                     text-center mt-5">
                        <div class="alert alert-warning font-weight-bold">
                            <i class="fa fa-exclamation" aria-hidden="true"></i> You dont have any order yet. Order Now <a href="/">here..</a>
                        </div>
                    </div>
                    @endforelse
                    @if(count($myCart->cart) > 0)
                        <div class="w-100" >
                                <button  type="button" id="btnPlaceOrder" class="btn btn-success float-right mr-5">Place Order</button>
                        </div>
                    @endif
                   
            </div>
        </form>
            @include('modal.shipping')
        </div>
    </div>
  
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            var sum = 0;
            var totalQuantity = 0;
            var total = 0;
            var itemQuantity = 0;
            var address = $('.address').text();
            var contact = $('#userContact').html();

            window.addCart = function(price, quantity, id){
                if($('#'+id).is(':checked')){
                    total = price * quantity;
                    itemQuantity = quantity;
                    totalQuantity = totalQuantity + itemQuantity;
                    sum = sum + total;
                }else{
                    total = price * quantity;
                    itemQuantity = quantity;
                    totalQuantity = totalQuantity - itemQuantity;
                    sum = sum - total;
                }
                $('#quantity').html(totalQuantity);
                $('.grandtotal').html("₱"+sum);
            }

            $('#btnPlaceOrder').click(function(){
                if(address == "No Shipping Address.." || contact == "Add Mobile" || itemQuantity == 0){
                    Toast.fire({
                        type: 'error',
                        title: 'Please Provide Shipping and Billing information'
                    });
                }else{
                    $('#placeOrderSubmit').submit();
                }
            });
         
        });
    </script>   
@endsection