<div class="p-5">
    <div class="text-center font-weight-bold">
        <h2>Other Bee Business</h2> 
    </div>
    <div class="row">
        <div class="col-md-4 img-container mb-2">
            <div class="font-weight-bold text-center text-dark" style="font-size:20px;">
                Solar and Led Lighting
            </div>
            <div>
                    <img class="img-fluid" src="{{ asset('img/45.png') }}" alt="">
            </div>
        </div>
        <div class="col-md-4 img-container mb-2">
            <div class="font-weight-bold text-center text-dark" style="font-size:20px;">
                E - Vehicles
            </div>
            <div>
                    <img class="img-fluid" src="{{ asset('img/46.png') }}" alt="">
            </div>
            
        </div>
        <div class="col-md-4 img-container mb-2">
            <div class="font-weight-bold text-center text-dark" style="font-size:20px;">
                Business Solution
            </div>
            <div>
                    <img class="img-fluid" src="{{ asset('img/47.png') }}" alt="">
            </div>
        </div>
    </div>

    <div class="row justify-content-end">
        <div class="col-md-4">
            <button class="btn orange w-100">VIEW ALL</button>
        </div>
    </div>

</div>