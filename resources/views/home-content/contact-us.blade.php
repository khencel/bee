<div class="row p-5">
    <div class="col-md-6">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="font-weight-bold">Contact Us</h2> 
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Please enter your Full Name">
                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Please Enter your Email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Contact Number</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Please enter your Contact Number">
                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Message</label>
                        <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="Please enter your Message"></textarea>
                    </div>
                    <button class="form-control btn orange">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <img class="img-fluid" style="box-shadow:0px 0px 5px black" src="{{ asset('img/Capture.png') }}" alt="">
                <div class="text-center h4 font-weight-bold p-2">
                   <a href="https://www.google.com/maps/place/23+Mark+St,+Cainta,+Rizal/@14.6131698,121.1125026,16.35z/data=!4m5!3m4!1s0x3397b85d0937cb11:0x5c75516cde78e892!8m2!3d14.6131879!4d121.1162952">23 Mark St, Cainta, Rizal</a> 
                </div>
            </div>
        </div>
    </div>
</div>