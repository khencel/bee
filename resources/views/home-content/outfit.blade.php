<div class="p-5">
    <div class="text-center font-weight-bold">
        <h2>BeeGoodies Outfit</h2> 
    </div>
    <div class="mt-4 font-weight-bold" style="font-size:20px;">
        Featured
    </div>
    <div class="row">
        <div class="col-md-3 col-6 img-container mb-2">
            <img class="img-fluid" src="{{ asset('img/25.png') }}" alt="">
            <div class="bottom-right mr-2" style="background-color:rgb(128, 128, 128)">
                <div class="p-2 text-center" style="line-height:17px;width:130px;">
                        <span style="font-size:30px;">₱199.00</span>
                        <div class="text-left pl-1">
                        <span>20%</span>
                            <span class="float-right">
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star deactive-star"></i>
                            </span> 
                        </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-3 col-6 img-container mb-2">
            <img class="img-fluid" src="{{ asset('img/26.png') }}" alt="">
            <div class="bottom-right mr-2" style="background-color:rgb(128, 128, 128)">
                <div class="p-2 text-center" style="line-height:17px;width:130px;">
                        <span style="font-size:30px;">₱199.00</span>
                        <div class="text-left pl-1">
                        <span>20%</span>
                            <span class="float-right">
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star deactive-star"></i>
                            </span> 
                        </div>
                        
                </div>
            </div>
        </div>
        <div class="col-md-3 col-6 img-container mb-2">
            <img class="img-fluid" src="{{ asset('img/27.png') }}" alt="">
            <div class="bottom-right mr-2" style="background-color:rgb(128, 128, 128)">
                <div class="p-2 text-center" style="line-height:17px;width:130px;">
                        <span style="font-size:30px;">₱199.00</span>
                        <div class="text-left pl-1">
                        <span>20%</span>
                            <span class="float-right">
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star deactive-star"></i>
                            </span> 
                        </div>
                        
                </div>
                
            </div>
        </div>
        <div class="col-md-3 col-6 img-container mb-2">
            <img class="img-fluid" src="{{ asset('img/28.png') }}" alt="">
            <div class="bottom-right mr-2" style="background-color:rgb(128, 128, 128)">
                <div class="p-2 text-center" style="line-height:17px;width:130px;">
                        <span style="font-size:30px;">₱199.00</span>
                        <div class="text-left pl-1">
                        <span>20%</span>
                            <span class="float-right">
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star active-star"></i>
                                <i class="fas fa-star deactive-star"></i>
                            </span> 
                        </div>
                        
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-end">
        <div class="col-md-3">
            <button class="btn orange w-100">VIEW ALL</button>
        </div>
    </div>

</div>