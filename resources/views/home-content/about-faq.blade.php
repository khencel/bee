<div class="row p-5">
        <div class="col-md-6 about-border">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1 class="font-weight-bold orange-font">About Us</h1> 
                    <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1 class="font-weight-bold orange-font">FAQs</h1> 
                    <ul>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>