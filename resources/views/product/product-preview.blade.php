@extends('layouts.master')

@section('content')
    <div class="h3 font-weight-bold recommended pl-3 mt-3">{{$item->name}}</div>
    <div class="row">
            <div class="col-md-6 bg-white" style="border-right:1px solid;border-color: rgb(225,225,225)">
                    <div class="p-4 font-weight-bold" style="line-height:17px;">
                        <div class="row ">
                            <div class="col-md-6 text-center">
                                <img class="img-fluid" id="primary-img" src="{{ asset('img/product-images/'.$item->product_images[0]->image) }}" alt="">
                            </div>
                            <div class="col-md-6 text-md-left text-sm-center text-center">
                                <div> 
                                        @php
                                            $rate = 3;
                                            $star = 5;
                                            $inactive = $star - $rate;   
                                        @endphp
                                        @for($i=1;$i<=$rate;$i++)
                                            <i class="fas fa-star active-star" style="font-size:15px;"></i>
                                        @endfor
                                        @for($i=1;$i<=$inactive;$i++)
                                            <i class="fas fa-star text-dark" style="font-size:15px;"></i>
                                        @endfor
                                </div>
                                <hr>
                                <div>
                                        <span class="h3" style="color:gray;">
                                            (₱{{number_format($item->price,2)}}) PRICE
                                        </span>
                                        <br>
                                            Description/Details
                                </div>
                                <hr>
                                <div class="row ">
                                    <div class="col-md-2 font-weight-bold gray">Colors:</div>
                                    <div class="col-md-5 ml-3">
                                            @if (count($item->product_details) < 1 || $item->product_details[0]->color == null)
                                                N/A
                                                <input type="hidden" id="prod_color" value="{{$item->product_details[0]->id}}">
                                            @else
                                                <select class="form-control m-auto" name="" id="prod_color">
                                                    @foreach ($item->product_details as $color)
                                                        <option value="{{$color->id}}">{{$color->color}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                    </div>
                                </div>
                                <div class="row mt-1">
                                        <div class="col-md-2 font-weight-bold gray">Size:</div>
                                        <div class="col-md-5 ml-3">
                                             @foreach ($item->product_details as $product)
                                                 {{$product->size}}
                                             @endforeach
                                        </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-2 font-weight-bold gray">Quantity:</div>
                                    <div class="col-md-9 ml-3">
                                            <button onclick="minusQuantity()"><i class="fas fa-minus" id="minus" ></i></button>
                                            <span><input disabled class="text-center font-weight-bold" type="text" value="0" id="quantity"  style="width:50px;"></span>
                                            <button onclick="addQuantity()"><i class="fas fa-plus" ></i></button>  
                                    </div>
                                </div>
                                <div class="owl-carousel owl-theme mt-3">
                                    @foreach ($item->product_images as $image)    
                                        <div class="p-2">
                                            <img class="img-fluid list-img p-2" style="box-shadow:0px 0px 5px black" src="{{ asset('img/product-images/'.$image->image) }}" alt="">
                                        </div>
                                    @endforeach   
                                </div>
                                <div>
                                    <button class="btn mt-4 green">BUY NOW</button>
                                    <button onclick="addToCart()" type="button" class="btn mt-4 orange">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="col-md-5 p-4 ">
                        <div class="">
                                <span class="font-weight-bold gray">
                                    Delivery Option
                                </span>
                        </div>
                        <div class="mt-2">
                            <span><i class="fas fa-map-marker-alt fa-2x"></i></span>
                            <span class="ml-3" style="font-size:17px;">
                                Metro Manila
                            </span>
                            <span class="float-right font-weight-bold green-font">CHANGE</span>
                        </div>
            
                        <div class="mt-4">
                            <span class="font-weight-bold gray">
                                Return & Warranty
                            </span>
                        </div>
                        <div class="mt-2">
                            <div class="row">
                                <div class="col-1">
                                        <span><i class="fas fa-exchange-alt"></i></i></span>
                                </div>
                                <div class="col-7">
                                    <span style="font-size:17px;line-height:10px;">
                                        7 days return to seller
                                        <p style="font-size:12px;">
                                                Change of mind is not applicable
                                        </p>
                                    </span>
                                </div>
                            </div>  
                        </div>
                </div>
    </div>

    <div class="row p-3">
        <div class="col-md-6 p-4">
                <h4 class=" font-weight-bold">
                        {{$item->name}}
                </h4>
                <div class="row">
                        <div class="col-md-6" style="line-height:15px;">
                            <span class="gray">
                                Brand
                            </span>  
                            <br>
                            No Brand
                        </div>
                        <div class="col-md-6" style="line-height:15px;">
                            <span class="gray">
                                SKU
                            </span>
                             
                            <br>
                            278437322_PH-418556177
                        </div>
                        <div class="col-md-6 mt-3" style="line-height:15px;">
                            <span class="gray">
                                Warranty Type
                            </span>
                             
                            <br>
                            Lazada refund warranty only
                        </div>
                        <div class="col-md-6 mt-3" style="line-height:15px;">
                            <span class="gray">
                                Warranty Period
                            </span>
                            <br>
                            7 Days
                        </div>
                    </div>
        </div>
        <div class="col-md-6 p-4">
               
                <h4 class="font-weight-bold">Product details of  {{$item->name}}</h4>
                @foreach ($item->product_details as $product)
                    {!!$product->description!!}
                @endforeach
                        
                
        </div>
    </div>       
    <div class="row pt-3 pl-5 pr-5 pb-2 " style="background-color:linen;">
        <div class="col-md-8 bg-white p-3" style="border-right:1px solid;border-color: rgb(225,225,225)">
            <div class="mb-5">
                <span class="font-weight-bold gray">
                    Delivery Option
                </span>
            </div>
        
        @for($x=1;$x<=5;$x++)
            <div>
                @php
                    $rate = 3;
                    $star = 5;
                    $inactive = $star - $rate;   
                @endphp
                @for($i=1;$i<=$rate;$i++)
                    <i class="fas fa-star active-star" style="font-size:15px;"></i>
                @endfor
                @for($i=1;$i<=$inactive;$i++)
                    <i class="fas fa-star text-dark" style="font-size:15px;"></i>
                @endfor

                <span class="float-right gray">1 week ago</span>
            </div>
            <div>
                <span class="gray">
                    by Lazada Customer
                </span>
            </div>
            <div>
                <span>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                </span>
            </div>
            <hr>
        @endfor    
        </div>
        <div class="col-md-4 bg-white p-3">
            <span class="font-weight-bold gray">
                Related Products
            </span>
            <div class="row">
                @for($i=0;$i<6;$i++)
                    <div class="col-md-6 p-3">
                        <img class="img-fluid" src="{{ asset('img/1.png') }}" alt="">
                    </div>
                @endfor
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
            var quantity = 0;
            var max = '{{$item->product_details[0]->quantity}}';

            $('#divFooter').show();

            $(".owl-carousel").owlCarousel({
                lazyLoad: true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:true,
                    }
                }
            });

            function addQuantity(){
                if(quantity < max){
                    var result =  ++quantity;
                    $('#quantity').val(result);
                }
                this.signColor();
            }

            function signColor(){
                if(quantity == 0){
                    $('#minus').css('color','gray');
                }else{
                    $('#minus').css('color','black');
                }
            }

            function minusQuantity(){
            if(quantity>1){
                    var result = --quantity;
                    $('#quantity').val(result);
            }
            this.signColor();
            }

            function addCart(){
                var product_details_id = $('#prod_color').val();
                var quantity = $('#quantity').val();

                $.ajax({
                    url:'/cart',
                    method:'POST',
                    data:{
                        product_details_id:product_details_id,
                        quantity:quantity
                    },
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(data){
                        Toast.fire({
                            type: 'success',
                            title: 'Naitapon na ang iyong item'
                        })
                    }
                });
            }
            
            function cartCounter(){
                $.ajax({
                    url:'/cartCounter',
                    method:'POST',
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(data){
                        if(data <= 0){
                            $('#cartQuantity').html();
                        }else{
                            $('#cartQuantity').html(data);
                        }
                    }
                });
            }

            cartCounter();
        
            function addToCart(){   
                $.ajax({
                    url:'/check',
                    method:'post',
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(data){
                        if(!data){
                            $('#loginmodal').modal('show');
                        }else{
                            Fire.$emit('afterCreate');
                            addCart();
                            cartCounter()
                            $('#quantity').val(0);
                        }
                    }
                });
            }  

            

            var currentImg = $('#primary-img').attr('src');

            
            $('.list-img').css('cursor','pointer');
            $('.list-img').click(function(){
                var img = $(this).attr('src');
                $('#primary-img').attr('src',img);
            })

            $('#prod_color').change(function(){
                var prod_details = $(this).val();
                $('#quantity').val(0);
                quantity = 0;
                $.ajax({
                    url:"/quantity",
                    method:"post",
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        prod_details:prod_details
                    },
                    success:function(data){
                        max = data;
                    }
                });
            });

            checkStock();

            $('#prod_color').change(function(){
                checkStock();
            });

            function checkStock(){
                var color_id = $('#prod_color').val();
                
                $.ajax({
                    url:'/check/quantity',
                    method:'post',
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        color_id:color_id
                    },
                    success:function(data){
                        max = data;
                        $('#quantity').val(0);
                    }
                });
            }

            

    </script>
@endsection