@extends('layouts.master')

@section('content')
    <div class="row p-5 justify-content-center">
        <div class="h1 w-100 recommended font-weight-bold">
            HappyBee Colors & Prints
        </div>
        <div class="w-100 h4 font-weight-bold mt-4">
            <span id="categories">Corporate Giveaways</span> 
            <span class="float-right">
                <ul class="nav nav-tabs" style="font-size:15px;">
                    <li class="nav-item">
                        <a class="nav-link active " href="#">Corporate Giveaways</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link green-font" href="#">Event Giveaways</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link green-font" href="#">Uniforms</a>
                    </li>
                </ul>
            </span>
        </div>
        <div class="row" id="post-data">
            @include('append.colors')
        </div>
        <div class="ajax-load text-center" style="display:none">
            <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
        </div>

       
    </div>
    
    <hr style="width:95%;">
@endsection

@section('script')
       <script>
            var page = 1;
            var count = '{{$count}}';
            
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    if(page != count){
                        page++;
                        loadMoreData(page);
                    } 

                    if(page == count){
                        $('#divFooter').show();
                    } 
                }
            });


            function loadMoreData(page){
            $.ajax(
                    {
                        url: 'colors?page=' + page,
                        type: "get",
                        beforeSend: function()
                        {
                            $('.ajax-load').show();
                        }
                    })
                    .done(function(data)
                    {
                        if(data.html == ""){
                            $('.ajax-load').html("No more records found");
                            
                            return;
                        }
                            $('.ajax-load').hide();
                            $("#post-data").append(data.html);
                        
                        
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError)
                    {
                        alert('server not responding...');
                    });

                
            }
    </script>
@endsection