@extends('layouts.master')

@section('content')
    <div class="row p-5 justify-content-center">
        <div class="h1 w-100 recommended font-weight-bold">
            {{$product->name}}
        </div>
        <div class="w-100 h4 font-weight-bold mt-4">
            <span class="float-right">
                <ul class="nav nav-tabs" style="font-size:15px;">
                   
                    @foreach($product->subcategories as $key => $subcategory)
                    
                        <?php 
                            $key = $key + 1;
                            $active = 'active';
                            // dd(url()->full());    
                            if($_GET['page'] == $key){
                        ?>
                            <li class="nav-item">
                                <a class="nav-link green-font active" href="?page={{$subcategory->id}}">{{$subcategory->name}}</a>
                            </li>
                        <?php
                           
                        }else{
                        ?>
                         <li class="nav-item">
                            <a class="nav-link green-font" href="?page={{$subcategory->id}}">{{$subcategory->name}}</a>
                        </li>
                        <?php
                        
                        }
                        ?>
                    @endforeach
                </ul>
            </span>
        </div>
        {{-- {{dd($product->subcategories)}} --}}
        @forelse($product->subcategories as $newKey => $subcategory)
            @if($_GET['page'] == $newKey + 1)
                <div class="w-100">
                    <b>{{$subcategory->name}}</b>
                </div>
            @endif
            @forelse($subcategory->products as $key => $product)
                {{-- {{dd($product)}} --}}
                <div class="col-lg-4 col-md-6 col-6">
                    <div class="row p-2">
                        <div class="col-lg-6 product-container text-center">
                            <a href="/product/{{$product->id}}" target="_blank">
                                    <img class="img-fluid" src="{{ asset('img/product-images/'.$product->product_images[0]->image) }}" alt="">
                            </a>
                        </div>
                        <div class="col-lg-6 bg-item-details">
                            <div class="h5 text-white mt-3">
                                {{str_limit($product->name,20)}}
                            </div>
                            <div>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            </div>
    
                            <div class="h5 mt-2">
                                Item Price ({{$product->price}})
                            </div>
                            <div class="text-center pb-3">
                                <a href="/product/{{$product->id}}" target="_blank"  class="orange btn w-75 mt-2" style="border:none;border-radius:10px;font-size:12px;" >Order Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                @if($newKey == 0)
                    <div class="h3 font-weight-bold">No Records...</div>
                @endif
            @endforelse
        @empty
            <div class="h3 font-weight-bold">No Records...</div>
        @endforelse
    </div>
    <hr style="width:95%;">
@endsection