@extends('layouts.master')

@section('content')
    <home-carousel></home-carousel>
    <sample-image :categories="{{ $home_product }}"></sample-image>
    <div class="col-md-12" id="post-data">
		@include('append.home_product')
    </div>
    {{-- <div class="ajax-load text-center" style="display:none">
	    <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
    </div> --}}
    <div id="lowerContent" >
        
        @include('home-content.other')
        @include('home-content.contact-us')
        @include('home-content.about-faq')
        @include('home-content.logo-list')
    </div>
@endsection

@section('script')
    
@endsection