<?php 
namespace App\Http\Traits;

trait ImageTrait {

    public static function createImage($photo, $products){
        if($photo != '/img/product-images/happy_image.png'){
            $imageData = $photo;
            $fileName = \Carbon\Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            $img = \Image::make($imageData)->fit(380, 380);
            $img->save(public_path('img/product-images/').$fileName);
            $products->product_images()->create(['image' => $fileName, 'is_main' => true]);
        }
    }
}