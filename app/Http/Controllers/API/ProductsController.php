<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\ProductImages;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(isset($_GET['search'])){
            return Products::with(['product_details', 'subcategory.category','product_images'])->where('name','like','%'.$_GET['search'].'%')->latest()->paginate(10);
        }else{
            return Products::with(['product_details', 'subcategory.category','product_images'])->latest()->paginate(10);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subcategory_id' => 'required',
            'name' =>'required|max:50|unique:products,name,'.$request->name,
            'price'=>'required|numeric|min:1',
            'quantity'=>'required|min:1',
            'description'=>'required',
            'imageData' => 'required'

        ]);

       $products = Products::create($request->only(['subcategory_id', 'name','price']));
       if(count($request->quantity) > 0 || count($request->color) > 0 || count($request->size) > 0){
            foreach($request->quantity as $key => $value){
                $products->product_details()->create([
                    'quantity' => $value,
                    'description' => $request->description,
                    'size' =>(count($request->size) < 1) ? NULL : $request->size[$key],
                    'color' => (count($request->color) < 1) ? NULL : $request->color[$key]
                ]);
            }
        }
       
        if($request->imageData){
            $image = explode('/', $request->imageData)[3];
            if($image != 'happy_image.png'){
                ProductImages::createImage($request->imageData, $products);
            }
        }

       return $products;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Products::with([
            'product_images' => function($query){
                return $query->main();
            },
            'subcategory' => function($query){
                return $query->with('category');
            },
            'product_details'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $products = Products::findOrfail($id);
        $products->product_details()->delete();
        $this->validate($request, [
            'subcategory_id' => 'required',
            'name' =>'required|max:50|max:255|unique:products,name,'.$products->id,
            'price'=>'required|numeric|min:1',
            'quantity'=>'required|min:1',
            'color'=>'required',
            'description'=>'required',
        ]);

        $image =  explode("/", $request->imageData)[3];
        $product = Products::with(['product_images' => function($photo){
            return $photo->main();
        }])->find($id);

        $product->update($request->only(['subcategory_id', 'name','price']));
        if(count($request->quantity) > 0 || count($request->color) > 0 || count($request->size) > 0){
            foreach($request->quantity as $key => $value){
                $products->product_details()->create([
                    'quantity' => $value,
                    'description' => $request->description,
                    'size' =>(count($request->size) < 1) ? NULL : $request->size[$key],
                    'color' => (count($request->color) < 1) ? NULL : $request->color[$key]
                ]);
            }
        }

        if(count($product->product_images) != 0){
            if($image != $product->product_images[0]->image){
                $imageData = $request->get('imageData');
                $fileName = \Carbon\Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
                $img = \Image::make($request->imageData)->fit(380, 380);
                $img->save(public_path('img/product-images/').$fileName);
                $product->product_images()->update(['image' => $fileName, 'is_main' => true]);
            }
        }else{
            ProductImages::createImage($request->imageData, $product);
        }
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Products = Products::findOrfail($id);
        $Products->delete();
    }
    public function search($data){
        $product = Products::with(['product_details', 'subcategory.category','product_images'])->where('name','like','%'.$data.'%')->latest()->paginate(10);
        return $product;
    }
}
