<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Category;
use Illuminate\Support\Facades\Route;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return SubCategory::with(['category' => function($query){
            return $query->get('name');
        }])->latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
           'name' => 'required|regex:/^[A-Za-z &]+$/u|max:191|unique:sub_categories,name,'.$request->name
        ]);

        return SubCategory::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return SubCategory::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcategory =  SubCategory::find($id);

        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|regex:/^[A-Za-z &]+$/u|max:191|unique:sub_categories,name,'.$subcategory->id
        ]);


        $subcategory->update($request->all());

        if($subcategory){
            return $subcategory->name;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub = SubCategory::findOrfail($id);
        $sub->delete();
    }

    public function changeStatus($id, $type){
        if($type == 'activate'){
            $data = true;
        }else{
            $data = false;
        }

        $subcategory = SubCategory::where('id', $id)->first();
        $subcategory->update([
            'is_active' => $data
        ]);

        return $subcategory;
    }
}
