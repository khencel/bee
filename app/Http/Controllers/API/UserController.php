<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return User::where('id', \Auth::user()->id)->first();
        return User::Where('type','!=','administrator')->get();
        // return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
           'email' => 'required|email|max:191|unique:users,email,'.$request->email,
           'password'=>'required|min:8|string',
           'type'=>'required',
        ]);

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request['password']);
        $user->type = $request->input('type');
        $user->save();

        return $user;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user =  user::find($id);

        $this->validate($request, [
            'name' => 'required',
           'email' => 'required|email|max:191|unique:users,email,'.$user->id,
           'password'=>'required|min:8|string',
           'type'=>'required',
        ]);

        $user->update($request->all());

        if ($user){
            return $user->name;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrfail($id);
        $user->delete();
    }


    public function changeUserStatus($id,$type){

        if($type == 'Activate'){
            $data = true;
        }else{
            $data = false;
        }

        $user = User::where('id', $id)->first();

        $user->update([
            'is_active' => $data
        ]);

        return $user;
    }
}
