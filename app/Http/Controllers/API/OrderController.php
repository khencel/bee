<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::with(['order_details' => function($query) {
            return $query->with('product_details');
        }])->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Order::with(['user.user_details', 'order_details' => function($query){
            return $query->with(['product_details'=> function($query){
                return $query->with('products.product_images');
            }]);
        }])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getOrderByStatus($status){
        return Order::with(['order_details' => function($query) {
            return $query->with('product_details');
        }])->where('status', $status)->paginate(10);
    }

    public function changeStatus($orderid, $status){
        $order = Order::find($orderid)->update([
            'status' => $status
        ]);
        
        return "Success";
    }
}
