<?php

namespace App\Http\Controllers;
use Socialite;
use Auth;
use App\User;
use Illuminate\Http\Request;

class LoginFacebookController extends Controller
{
    private $path;

    public function redirectToProvider()
    {
        \Session::put('path', $_GET['path']);
        return Socialite::driver('facebook')->redirect();
    }

    

    public function handleProviderCallback()
    {  
        try{
            $user = Socialite::driver('facebook')->user();
        } catch(Exception $e){
            return redirect('login/facebook');
        }
        
        $authUser = $this->findOrCreateUser($user);
        Auth::login($authUser, true);
        $url =  redirect()->getUrlGenerator()->previous();
        $redirect = explode("/", $url);
        if($redirect[2] != 'localhost:8000'){  // change to happybee.com.ph
           return redirect()->to(\Session::get('path'));
        }else{
            return redirect()->to(redirect()->getUrlGenerator()->previous());
        }
    }

    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('facebook_id', $facebookUser->id)->first();
        if ($authUser){
            return $authUser;
        }
        return User::create([
            'name' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
            'avatar' => $facebookUser->avatar,
            'is_active' => true
        ]);
    }
}
