<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use App\ProductDetails;
use App\Category;
use App\SubCategory;



class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Products::all());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $item = Products::with('product_details','product_images')->findOrFail($id);
        return view('product.product-preview',compact('item'));
        // dd($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
    }

    public function prod_quantity(Request $request){
        $prod_quant = ProductDetails::find($request->prod_details);
        return $prod_quant->quantity;
    }

    public function showProductByCategories($page, Request $request){
        
        
        if(!isset($request->page)){
     
            $product = Category::with(['subcategories' => function($query){
                return $query->with(['products' => function($query){
                    return $query->with('product_details','product_images');
                }]);
            }])->find($page);
        }else{
            $product = Category::with(['subcategories' => function($query) use ($request) {
                return $query->with(['products' => function($query) use ($request){
                    return $query->where('subcategory_id', $request->get('page'))->with('product_details','product_images');
                }]);
            }])->find($page);
           
        }
        
        
        return view('content.product',compact('product'));
        
    }

}
