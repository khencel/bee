<?php

namespace App\Http\Controllers;

use App\ProductDummy;
use Illuminate\Http\Request;

class ProductDummyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('content.corporate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductDummy  $productDummy
     * @return \Illuminate\Http\Response
     */
    public function show(ProductDummy $productDummy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductDummy  $productDummy
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductDummy $productDummy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDummy  $productDummy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductDummy $productDummy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDummy  $productDummy
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductDummy $productDummy)
    {
        //
    }

    public function fetchProduct(Request $request){
        
        $product = ProductDummy::paginate(6);
        $product_count = ProductDummy::all();

        $count = ceil(sizeof($product_count)/6);

        if($request->ajax()){
            $view = view('append.colors',compact('product'))->render();
            return response()->json(['html'=>$view]);
        }
        return view('content.corporate',compact('product','count'));
             

        // $product = ProductDummy::latest()->skip($request->start)->take($request->limit)->get();
        // // $product = ProductDummy::all();
       
        // return response()->json(['element' => view('append.colors', compact('product'))->render()]);
    }
}
