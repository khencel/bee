<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Cart;
use App\User;
use App\ProductDetails;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        foreach($request->cartDetails as $key => $details){
            foreach($details as $details){
                Cart::where('id', $details)->update(['quantity' => $request->cartQuantity[$key]]);
            }
        }

        // return redirect()->to('checkout/items');
        // return response()->json(['data' => $array]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function addToCart(Request $request){
        
            $request['user_id'] = \Auth::user()->id;
            $currentQuantity = Cart::where('product_details_id',$request->product_details_id)->first('quantity');
            if($currentQuantity){
                $request['quantity'] = $currentQuantity->quantity + (int) $request->quantity;
            }
            
            
            Cart::updateOrCreate(['product_details_id'=>$request->product_details_id],$request->only(['user_id','product_details_id','quantity']));
        
    }

    public function countCart(){
        if(isset(\Auth::user()->id)){
            $count = Cart::where('user_id',\Auth::user()->id)->get();
            return count($count);
        }
    }

    public function checkAvailableQuantity(Request $request){
        $currentQuantity = ProductDetails::find($request->color_id);
        $cartQuantity = Cart::where('product_details_id',$request->color_id)
                            ->where('user_id', \Auth::user()->id)
                            ->first('quantity');
        return $currentQuantity->quantity - $cartQuantity->quantity;
    }

    public function checkOut(){
        $myItem = array();
        $myCart = User::with(['cart' =>function($query){
            return $query->with(['product_details' => function($query){
                return $query->with(['products' => function($query){
                    return $query->with('product_images');
                }]);
            }]);
        }])->find(\Auth::user()->id);

        foreach($myCart->cart as $item){
            $myItem[] = $item->quantity * $item->product_details->products->price;
        }
        
        
        return view('checkout.checkout_home',compact('myCart','myItem'));
    }

    public function placeorder(Request $request){
        $this->validate($request, [
            'cart' => 'required'
        ]);
        if(isset($request->cart)){
            $order = Order::create([
                'order_no' => "HappyBee" . $this->generateRandomString(5),
                'user_id' => \Auth::user()->id,
                'status' => 'pending'
            ]);
            
            foreach($request->cart as $cart){
                $cartDetails = Cart::find($cart);
                $product = ProductDetails::with('products')->where('id', $cartDetails->product_details_id)->first();
                $price = $cartDetails->quantity * $product->products->price;
                $order->order_details()->create([
                    'product_details_id' => $product->id,
                    'quantity' => $cartDetails->quantity,
                    'price' => $price
                ]);
                $product->update([
                    'quantity' => $product->quantity - $cartDetails->quantity
                ]);
                Cart::destroy($cart);
            }
        }
       return redirect("/+checkout");
       
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function showOrders($id){
        $orders =  Order::with(['order_details' => function($query){
            return $query->with(['product_details' => function($query){
                return $query->with('products.product_images');
            }]);
        }])->where('user_id', $id)->get();

        return view('order.show', compact('orders'));
    }
}
