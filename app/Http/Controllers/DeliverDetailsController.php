<?php

namespace App\Http\Controllers;

use App\DeliverDetails;
use Illuminate\Http\Request;

class DeliverDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliverDetails  $deliverDetails
     * @return \Illuminate\Http\Response
     */
    public function show(DeliverDetails $deliverDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliverDetails  $deliverDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliverDetails $deliverDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliverDetails  $deliverDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliverDetails $deliverDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliverDetails  $deliverDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliverDetails $deliverDetails)
    {
        //
    }
}
