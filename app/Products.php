<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Products extends Model
{
    use SoftDeletes, LogsActivity;

    protected $guarded = [];
    protected static $logUnguarded = true;
    
    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class, 'subcategory_id', 'id');
    }

    public function product_details(){
        return $this->hasMany(ProductDetails::class, 'product_id', 'id');
    }

    public function product_images(){
        return $this->hasMany(ProductImages::class, 'product_id', 'id');
    }

    public function order_details(){
        return $this->hasMany(OrderDetails::class);
    }
}
