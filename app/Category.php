<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use SoftDeletes, LogsActivity;

    
    protected $guarded = [];

    protected static $logUnguarded = true;

    public function subcategories(){
        return $this->hasMany(SubCategory::class, 'category_id', 'id');
    }

    
}
