<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\ImageTrait;

class ProductImages extends Model
{
    use SoftDeletes, ImageTrait;

    protected $guarded = [];
    public function products(){
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    public function scopeMain($query){
        return $query->where('is_main', true);
    }
}
