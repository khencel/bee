<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $guarded = [];
    public function orders(){
        return $this->belongsTo(Order::class);
    }

    public function product_details(){
        return $this->belongsTo(ProductDetails::class, 'product_details_id', 'id');
    }
}
