<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $guarded = [];

    public function order_details(){
        return $this->hasMany(OrderDetails::class);
    }
    public function payment(){
        return $this->hasMany(Payment::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
