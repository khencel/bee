<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "cart";
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product_details(){
        return $this->belongsTo(ProductDetails::class);
    }
}
