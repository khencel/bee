<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    

    protected $guarded = [];
    protected $table = 'product_details';
    public function products()
    {

        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    public function cart(){
        return $this->hasMany(Cart::class);
    }

    public function order_details(){
        return $this->hasMany(OrderDetails::class, 'product_details_id', ' id');
    }
}
