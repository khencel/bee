<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    
    public function products()
    {
        return $this->hasMany(Products::class, 'subcategory_id', 'id');
    }

    public function scopeActive($query){
        return $query->where('is_active', true);
    }
}
