(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loading: false,
      showloader: false,
      overlayLoader: false,
      labelLoader: "Loading...",
      products: {},
      productDetails: {},
      mode: 'addMode'
    };
  },
  methods: {
    createProduct: function createProduct() {
      Fire.$emit('openAdd');
      this.mode = 'addMode';
      this.showloader = true;
      $("#productModal").modal('show');
      this.showloader = false; // this.$refs.modalProduct.assignImageData();
    },
    fetchAllProduct: function fetchAllProduct() {
      var _this = this;

      this.loading = true;
      axios.get('api/products').then(function (_ref) {
        var data = _ref.data;
        _this.products = data.data;
        _this.loading = false;
      });
    },
    editProduct: function editProduct(id) {
      this.mode = 'editMode'; // this.showloader = true

      $("#productModal").modal('show');
      axios.get('api/products/' + id).then(function (_ref2) {
        var data = _ref2.data;
        Fire.$emit('openEdit', data);
      });
    },
    deleteProduct: function deleteProduct(id) {// $("#deleteProductModal").modal('show')
      // Swal.fire({
      //   title: 'Are you sure?',
      //   text: "You'll be able to revert it later!",
      //   type: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonColor: '#d33',
      //   confirmButtonText: 'Yes it!'
      //   }).then((result) => {
      //   if (result.value) {
      //   }
      // })
    },
    openMassUpload: function openMassUpload(id) {
      this.$refs.imagesModal.loadProduct(id);
    }
  },
  created: function created() {
    var _this2 = this;

    this.fetchAllProduct();
    Fire.$on("successCreated", function () {
      _this2.fetchAllProduct();
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container" },
    [
      _c("loading", {
        attrs: {
          show: _vm.showloader,
          label: _vm.labelLoader,
          overlay: _vm.overlayLoader
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "row justify-content-center" }, [
        _c("div", { staticClass: "col-12 mt-4" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-header" }, [
              _c("h3", { staticClass: "card-title" }, [
                _vm._v("Products Table")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card-tools" }, [
                _c("div", { staticClass: "input-group input-group-sm" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-success",
                      on: { click: _vm.createProduct }
                    },
                    [
                      _c("i", { staticClass: "fas fa-plus" }),
                      _vm._v(" Add Product")
                    ]
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-body table-responsive p-0" }, [
              _c("table", { staticClass: "table table-hover" }, [
                _vm._m(0),
                _vm._v(" "),
                _c(
                  "tbody",
                  { staticClass: "text-center" },
                  [
                    _vm.loading
                      ? _c("tr", [
                          _c(
                            "td",
                            { attrs: { colspan: "6" } },
                            [
                              _c("circle-spinner"),
                              _c("br"),
                              _vm._v(" "),
                              _c("i", { staticClass: "font-weight-bold" }, [
                                _vm._v("Loading...")
                              ])
                            ],
                            1
                          )
                        ])
                      : _vm.products.length < 1
                      ? _c("tr", [_vm._m(1)])
                      : _vm._l(_vm.products, function(product, index) {
                          return _c("tr", { key: product.id }, [
                            _c("td", [_vm._v(_vm._s(index + 1))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(product.name))]),
                            _vm._v(" "),
                            _c("td", { staticClass: "font-weight-bold" }, [
                              _vm._v(
                                _vm._s(product.subcategory.category.name) + " "
                              ),
                              _c("i", {
                                staticClass: "fa fa-arrow-right",
                                attrs: { "aria-hidden": "true" }
                              }),
                              _vm._v(" " + _vm._s(product.subcategory.name))
                            ]),
                            _vm._v(" "),
                            !product.product_details
                              ? _c(
                                  "td",
                                  {
                                    staticClass: "text-danger font-weight-bold"
                                  },
                                  [_vm._v("0")]
                                )
                              : product.product_details.quantity < 5
                              ? _c(
                                  "td",
                                  {
                                    staticClass: "font-weight-bold",
                                    staticStyle: { color: "#E16600" }
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(product.product_details.quantity)
                                    )
                                  ]
                                )
                              : _c(
                                  "td",
                                  {
                                    staticClass: "text-success font-weight-bold"
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(product.product_details.quantity)
                                    )
                                  ]
                                ),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(_vm._s(product.product_images.length))
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "badge badge-success p-2 text-white letter-spacing",
                                  on: {
                                    click: function($event) {
                                      return _vm.openMassUpload(product.id)
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-upload",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v("  Add More Images ")
                                ]
                              ),
                              _vm._v(" "),
                              _c("span"),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "badge badge-info p-2 ml-2 text-white letter-spacing",
                                  on: {
                                    click: function($event) {
                                      return _vm.editProduct(product.id)
                                    }
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-pencil-alt" }),
                                  _vm._v("Edit ")
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "badge badge-danger p-2 ml-2 text-white letter-spacing",
                                  on: {
                                    click: function($event) {
                                      return _vm.deleteProduct(product.id)
                                    }
                                  }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-times" }),
                                  _vm._v(" Delete ")
                                ]
                              )
                            ])
                          ])
                        })
                  ],
                  2
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("product-modal", {
        ref: "modalProduct",
        attrs: { mode: _vm.mode, productDetails: _vm.productDetails },
        on: { createdSuccess: _vm.fetchAllProduct }
      }),
      _vm._v(" "),
      _c("delete-product"),
      _vm._v(" "),
      _c("image-modal", {
        ref: "imagesModal",
        on: { createdSuccess: _vm.fetchAllProduct }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "text-center" }, [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Name")]),
        _vm._v(" "),
        _c("th", [
          _vm._v("Category "),
          _c("i", { staticClass: "fa fa-arrow-right" }),
          _vm._v(" Subcategory")
        ]),
        _vm._v(" "),
        _c("th", [_vm._v("Quantity")]),
        _vm._v(" "),
        _c("th", [_vm._v("# of Images")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", { attrs: { colspan: "6" } }, [
      _c("i", [_vm._v("No Record...")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/products/ProductComponent.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/products/ProductComponent.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true& */ "./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true&");
/* harmony import */ var _ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4e42f1fe",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/products/ProductComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/products/ProductComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/products/ProductComponent.vue?vue&type=template&id=4e42f1fe&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductComponent_vue_vue_type_template_id_4e42f1fe_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);