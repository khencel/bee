<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CategoryController@homeProduct');


// testing purposes
Route::post('/log','CategoryController@testing');
Route::post('/check','CategoryController@checking');
Route::post('/quantity','ProductsController@prod_quantity');
Route::post('/cartCounter','OrderController@countCart');

Route::get('login/facebook', 'LoginFacebookController@redirectToProvider');
Route::get('login/facebook/callback', 'LoginFacebookController@handleProviderCallback');

Route::post('/cart','OrderController@addToCart');
// end testing
// http://localhost:3000/login/facebook/callback


Route::get('/productCategory/{page}', 'ProductsController@showProductByCategories');
Route::get('/colors2', 'ProductDummyController@fetchProduct');
Route::resource('/product','ProductsController');
Route::post('/login/customer','API\CustomerController@loginCustomer');

Route::get('/foodies', function () {
    return view('content.foodies');
});

Route::get('/outfit', function () {
    return view('content.homePage');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();



// Route::get('/home', 'HomeController@index')->name('home');
Route::get('{path}', 'HomeController@index')->where('path', '([A-z\d\/_.]+)?');

// Route::resource('/products/getAll', 'ProductsController');
Route::post('check/quantity','OrderController@checkAvailableQuantity');
Route::post('placeorder','OrderController@placeorder');
Route::resource('order', 'OrderController');
Route::get('/+checkout','OrderController@checkOut');
Route::resource('addshipping','UserDetailController');
Route::get('/+order/{id}', 'OrderController@showOrders');

