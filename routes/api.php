<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api', 'role:admin']], function(){
    Route::resources([
        'subcategory' => 'API\SubCategoryController',
        'products' => 'API\ProductsController',
        'product-image' => 'API\ProductImagesController',
        'user' => 'API\UserController',
        'customer' => 'API\CustomerController',
        'cart' => 'API\CartController',
        'order' => 'API\OrderController',
        'activity' => 'API\ActivityController'
    ]);
    Route::resource('category', 'API\CategoryController', ['except'=>['index']]);
    Route::get('order/get/{status}', 'API\OrderController@getOrderByStatus');
    Route::get('order/{orderid}/{status}',  'API\OrderController@changeStatus');
    Route::resource('category','API\CategoryController',['except' => ['index']]);
});
Route::get('category', 'API\CategoryController@index');
Route::resource('cart', 'API\CartController');
Route::resource('product-details', 'API\ProductDetailsController', ['only' => ['show']]);
Route::get('product-details/{id}/delete-img', 'API\ProductDetailsController@deleteImage');
Route::get('subcategory/{id}/changestatus/{type}', 'API\SubCategoryController@changeStatus');
Route::get('user/{id}/status/{type}', 'API\UserController@changeUserStatus');
Route::get('category/getCategoryId/{id}', 'API\CategoryController@getCategoryId');
//delete
Route::get('users/{id}', 'API\UserController@destroy');
Route::get('product/{id}','API\ProductsController@destroy');
Route::get('subcategorys/{id}','API\SubCategoryController@destroy');

//search
Route::get('search/{data}','API\ProductsController@search');
Route::post('discount','API\ProductsController@discount');

