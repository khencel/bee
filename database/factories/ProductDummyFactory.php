<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ProductDummy;
use Faker\Generator as Faker;

$factory->define(ProductDummy::class, function (Faker $faker) {
    return [
        'product' => $faker->name
    ];
});
