<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'subcategory_id' => rand(3, 8),
        'name' => $faker->name
    ];
});
